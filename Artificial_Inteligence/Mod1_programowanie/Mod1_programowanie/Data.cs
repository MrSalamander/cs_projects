﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Mod1_programowanie
{
	class Data
	{
		//C:\Users\Aleksander\Documents\cs_projects\Artificial_Inteligence\Mod1_programowanie\\Mod1_programowanie
		const string path = @"..\..\part1_data\DressesSales.txt";

		private List<dataTXT> soldDressesList;
		public Data()
		{

			soldDressesList = Read(path);               // wczytuje
			soldDressesList = Shuffle(soldDressesList); //tasuje
			PrintList(soldDressesList);

			Console.WriteLine();
			Console.WriteLine("@@@@@@@@@@  - Normalized -  @@@@@@@@@@@@@@@@@");
			Console.WriteLine("ID:        Jan:       Feb:       Mar:");
			Console.WriteLine();

			List<dataTXT> soldDressesListNormalized = Normalize_Mean(soldDressesList);
			PrintList(soldDressesListNormalized);

			Console.WriteLine();
			Console.WriteLine("@@@@@@@@@@   - Standardized -  @@@@@@@@@@@@@@@");
			Console.WriteLine("ID:        Jan:       Feb:       Mar:");
			Console.WriteLine();

			List<dataTXT> soldDressesListStandardized = Standardize(soldDressesList);
			PrintList(soldDressesListStandardized);

		}
		

		#region required methods for ex. 1
		private List<dataTXT> Read(string path) 
		{
			List<dataTXT> tmp = new List<dataTXT>();
			try
			{
				string[] lines = System.IO.File.ReadAllLines(path);
				foreach(string line in lines)
				{
					string[] separatedLine = Regex.Replace(line, @"\s+", " ").Split(' ');
					double[] constructData;
					try
					{
						List<double> temp = new List<double>();
						foreach (string part in separatedLine)
						{
							temp.Add(double.Parse(part));
						}
						constructData = temp.ToArray();
						tmp.Add(new dataTXT(constructData));
					} catch (Exception e)
					{
						Console.WriteLine("wrong data, parsing failed " + e.Message);
					}
				}
			}
			catch (Exception e) { Console.WriteLine(e.Message); }
			return tmp;
		}
		private List<dataTXT> Shuffle(List<dataTXT> list)
		{
			List<dataTXT> tmp = list;
			dataTXT temp;
			Random rand = new Random();
			int l,r;
			for(int i = 0; i < list.Count(); i++)
			{
				l = rand.Next(list.Count());
				r = rand.Next(list.Count());
				temp = tmp[l];
				tmp[l] = tmp[r];
				tmp[r] = temp;
			}
			return tmp;
		}
		private List<dataTXT> Normalize_Mean(List<dataTXT> list)
		{
			List<dataTXT> normalized = new List<dataTXT>();
			double min_jan, min_feb, min_mar, max_jan, max_feb, max_mar;
			
			min_jan = list.Min(x => x.sales_jan);
			min_feb = list.Min(x => x.sales_feb);
			min_mar = list.Min(x => x.sales_mar);

			max_jan = list.Max(x => x.sales_jan);
			max_feb = list.Max(x => x.sales_feb);
			max_mar = list.Max(x => x.sales_mar);

			// new_min = 0 
			// new_max = 1
			foreach (dataTXT dataObject in list)
			{
				double newJan = Math.Round(((dataObject.sales_jan - min_jan) / (max_jan - min_jan)),3);
				double newFeb = Math.Round(((dataObject.sales_feb - min_feb) / (max_feb - min_feb)),3);
				double newMar = Math.Round(((dataObject.sales_mar - min_mar) / (max_mar - min_mar)),3);
				normalized.Add(new dataTXT(dataObject.dress_ID,newJan,newFeb,newMar));
			}
			return normalized;
		}
		private List<dataTXT> Normalize_MinMax(List<dataTXT> list, double newMin, double newMax)
		{
			List<dataTXT> normalized = new List<dataTXT>();
			double min_jan, min_feb, min_mar, max_jan, max_feb, max_mar;

			min_jan = list.Min(x => x.sales_jan);
			min_feb = list.Min(x => x.sales_feb);
			min_mar = list.Min(x => x.sales_mar);

			max_jan = list.Max(x => x.sales_jan);
			max_feb = list.Max(x => x.sales_feb);
			max_mar = list.Max(x => x.sales_mar);

			// new_min = 0 
			// new_max = 1
			foreach (dataTXT dataObject in list)
			{
				double newJan = Math.Round(((dataObject.sales_jan - min_jan) / (max_jan - min_jan) * (newMax - newMin) + newMin), 3);
				double newFeb = Math.Round(((dataObject.sales_feb - min_feb) / (max_feb - min_feb) * (newMax - newMin) + newMin), 3);
				double newMar = Math.Round(((dataObject.sales_mar - min_mar) / (max_mar - min_mar) * (newMax - newMin) + newMin), 3);
				normalized.Add(new dataTXT(dataObject.dress_ID, newJan, newFeb, newMar));
			}
			return normalized;
		}
		private List<dataTXT> Standardize(List<dataTXT> list)
		{
			double avg_jan = list.Average(x => x.sales_jan);
			double avg_feb = list.Average(x => x.sales_feb);
			double avg_mar = list.Average(x => x.sales_mar);
			double stan_jan, stan_feb, stan_mar;
			double[] variation = Variation(list);
			List<dataTXT> standardized = new List<dataTXT>();
			foreach(var obj in list)
			{
				stan_jan = Math.Round(((obj.sales_jan - avg_jan) / Math.Sqrt(variation[0])), 5);
				stan_feb = Math.Round(((obj.sales_feb - avg_feb) / Math.Sqrt(variation[1])), 5);
				stan_mar = Math.Round(((obj.sales_mar - avg_mar) / Math.Sqrt(variation[2])), 5);
				standardized.Add(new dataTXT(obj.dress_ID, stan_jan, stan_feb, stan_mar));
			}
			return standardized;
		}
		#endregion

		private double[] Variation(List<dataTXT> list)
		{
			double[] variation = new double[3];
			int amount = list.Count();

			double avg_jan = list.Average(x => x.sales_jan);
			double avg_feb = list.Average(x => x.sales_feb);
			double avg_mar = list.Average(x => x.sales_mar);

			foreach (var obj in list)
			{
				variation[0] += Math.Pow((obj.sales_jan - avg_jan), 2);
				variation[1] += Math.Pow((obj.sales_feb - avg_feb), 2);
				variation[2] += Math.Pow((obj.sales_mar - avg_mar), 2);
			}
			variation[0] = variation[0] / amount;
			variation[1] = variation[1] / amount;
			variation[2] = variation[2] / amount;
			return variation;
		}
		public void PrintList(List<dataTXT> list)
		{
			foreach (dataTXT line in list) Console.WriteLine(line.ToString());	
		}

	}
}
