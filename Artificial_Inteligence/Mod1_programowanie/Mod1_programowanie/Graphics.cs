﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Mod1_programowanie
{
	class Graphics
	{
		
		Bitmap img;
		int[,] R, G, B;

		public Graphics(string path)
		{
			img = new Bitmap(path);
			R = new int[img.Width, img.Height];
			G = new int[img.Width, img.Height];
			B = new int[img.Width, img.Height];

			createMatrix();

			if (gauss_Filtr()) Console.WriteLine("Gauss filtr was applied succesfully"); else { Console.WriteLine("applying Gauss filtr failed!"); };
			if (blur_Filtr()) Console.WriteLine("Blur filtr was applied succesfully"); else { Console.WriteLine("applying blur filtr failed!"); };
			if (sharpen_Filtr()) Console.WriteLine("Sharpen filtr was applied succesfully"); else { Console.WriteLine("applying sharpen filtr failed!"); };
		}


		#region required methods for ex. 2.
		private void createMatrix()
		{
			for (int i = 0; i < img.Width; i++)
			{
				for (int j = 0; j < img.Height; j++)
				{
					R[i, j] = img.GetPixel(i, j).R;
					G[i, j] = img.GetPixel(i, j).G;
					B[i, j] = img.GetPixel(i, j).B;
				}
			}
		}

		public bool gauss_Filtr()
		{
			Bitmap imgBlur = img;
			int[,] Rb = R, Gb = G, Bb = B;
			int[,] mask = { { 1, 2, 1 }, { 2, 4, 2 }, { 1, 2, 1 } };
			//			1  2  1
			//			2  4  2     Mask 3x3
			//			1  2  1
			int sumR = 0, sumG = 0, sumB = 0;
			string savePath = @"C:\Users\" + Environment.UserName + @"\Desktop\gaussedImage.png";

			for (int i = 0; i < img.Width - 2; i++)
			{
				for (int j = 0; j < img.Height - 2; j++)
				{
					sumR = sumG = sumB = 0;
					for (int k = 0; k <= mask.GetUpperBound(0); k++)
					{
						for (int l = 0; l <= mask.GetUpperBound(0); l++)
						{
							sumR += Rb[i + k, j + l] * mask[k, l];
							sumG += Gb[i + k, j + l] * mask[k, l];
							sumB += Bb[i + k, j + l] * mask[k, l];
						}
					}
					sumR /= 16;
					sumG /= 16;
					sumB /= 16;
					imgBlur.SetPixel(i + 1, j + 1, Color.FromArgb(sumR, sumG, sumB));
				}
			}
			imgBlur.Save(savePath, System.Drawing.Imaging.ImageFormat.Png);
			return true;
		}

		public bool blur_Filtr()
		{
			Bitmap imgBlur = img;
			int[,] Rb = R, Gb = G, Bb = B;
			int[,] mask = { { 1, 1, 1 }, { 1, 1, 1 }, { 1, 1, 1 } };
			//			1  1  1
			//			1  1  1     Mask 3x3
			//			1  1  1
			int sumR = 0, sumG = 0, sumB = 0;
			string savePath = @"C:\Users\" + Environment.UserName + @"\Desktop\bluredImage.png";

			for (int i = 0; i < img.Width - 2; i++)
			{
				for (int j = 0; j < img.Height - 2; j++)
				{
					sumR = sumG = sumB = 0;
					for (int k = 0; k <= mask.GetUpperBound(0); k++)
					{
						for (int l = 0; l <= mask.GetUpperBound(0); l++)
						{
							sumR += Rb[i + k, j + l] * mask[k, l];
							sumG += Gb[i + k, j + l] * mask[k, l];
							sumB += Bb[i + k, j + l] * mask[k, l];
						}
					}
					sumR /= 9;
					sumG /= 9;
					sumB /= 9;
					imgBlur.SetPixel(i + 1, j + 1, Color.FromArgb(sumR, sumG, sumB));
				}
			}
			imgBlur.Save(savePath, System.Drawing.Imaging.ImageFormat.Png);
			return true;
		}

		public bool sharpen_Filtr()
		{
			Bitmap imgBlur = img;
			int[,] Rb = R, Gb = G, Bb = B;
			int[,] mask = { { 0, -1, 0 }, { -1, 5, -1 }, { 0, -1, 0 } };
			//			 0  -1   0
			//			-1   5  -1     Mask 3x3
			//			 0  -1   0
			int sumR = 0, sumG = 0, sumB = 0;
			string savePath = @"C:\Users\" + Environment.UserName + @"\Desktop\bluredImage.png";

			for (int i = 0; i < img.Width - 2; i++)
			{
				for (int j = 0; j < img.Height - 2; j++)
				{
					sumR = sumG = sumB = 0;
					for (int k = 0; k <= mask.GetUpperBound(0); k++)
					{
						for (int l = 0; l <= mask.GetUpperBound(0); l++)
						{
							sumR += Rb[i + k, j + l] * mask[k, l];
							sumG += Gb[i + k, j + l] * mask[k, l];
							sumB += Bb[i + k, j + l] * mask[k, l];
						}
					}
					if (sumR > 255) sumR = 255;
					else if (sumR < 0) sumR = 0;

					if (sumG > 255) sumG = 255;
					else if (sumG < 0) sumG = 0;

					if (sumB > 255) sumB = 255;
					else if (sumB < 0) sumB = 0;
					imgBlur.SetPixel(i + 1, j + 1, Color.FromArgb(sumR, sumG, sumB));
				}
			}
			imgBlur.Save(savePath, System.Drawing.Imaging.ImageFormat.Png);
			return true;
		}
		#endregion


	}
}
