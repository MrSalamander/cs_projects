﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mod1_programowanie
{
	class dataTXT
	{
		public double dress_ID;
		public double sales_jan;
		public double sales_feb;
		public double sales_mar;
		public dataTXT(double dress_ID, double sales_jan, double sales_feb, double sales_mar)
		{
			this.dress_ID = dress_ID;
			this.sales_jan = sales_jan;
			this.sales_feb = sales_feb;
			this.sales_mar = sales_mar;
		}
		public dataTXT(double[] tab)
		{
			this.dress_ID = tab[0];
			this.sales_jan = tab[1];
			this.sales_feb = tab[2];
			this.sales_mar = tab[3];
		}

		public override string ToString()
		{
			StringBuilder myStringBuilder = new StringBuilder();
			myStringBuilder.Append(dress_ID.ToString().PadRight(10, ' '));
			myStringBuilder.Append(" ");
			myStringBuilder.Append(sales_jan.ToString().PadRight(10, ' '));
			myStringBuilder.Append(" ");
			myStringBuilder.Append(sales_feb.ToString().PadRight(10, ' '));
			myStringBuilder.Append(" ");
			myStringBuilder.Append(sales_mar.ToString().PadRight(10, ' '));
			return myStringBuilder.ToString();
		}
	}
}
