﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using NAudio.CoreAudioApi;

namespace Sound
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
			MMDeviceEnumerator enumerator = new MMDeviceEnumerator();
			var devices = enumerator.EnumerateAudioEndPoints(DataFlow.All, DeviceState.Active);
			comboBox1.Items.AddRange(devices.ToArray());
			progressBar1.ForeColor = Color.Red;
			progressBar1.BackColor = Color.AliceBlue;
		}

		private void timer1_Tick(object sender, EventArgs e)
		{
			if(comboBox1.SelectedItem != null)
			{
				var device = (MMDevice)comboBox1.SelectedItem;
				progressBar1.Value = (int)Math.Round(device.AudioMeterInformation.MasterPeakValue * 100);
			}
		}
	}
}
