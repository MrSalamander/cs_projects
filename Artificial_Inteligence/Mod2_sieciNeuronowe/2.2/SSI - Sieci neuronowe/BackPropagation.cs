﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSI___Sieci_neuronowe
{
    class BackPropagation
    {
        // hidden neuron weight:
        decimal hnw11 = 0.11m, hnw12 = 0.21m, hnw21 = 0.12m, hnw22 = 0.08m;

        // output weight: 
        decimal ow11 = 0.12m, ow21 = 0.12m, lr = 0.5m;

        public void train(int[] tab1, decimal target)
        {
            // backpropagation start

            decimal in1 = tab1[0], in2 = tab1[1], hn1, hn2, out1, error, error_hn1, error_hn2;
            hn1 = (in1 * hnw11 ) + ( in2 * hnw21 );
            hn2 = ( in1 * hnw12 ) + ( in2 * hnw22 );
            out1 = ( hn1 * ow11 ) + ( hn2 * ow21 );
            show("\nBefore learn: ");
            show();

            error = (target - out1) * (target - out1);

            error_hn1 = ow11 / (ow11 + ow21) * error;
            error_hn2 = ow21 / (ow11 + ow21) * error;

            ow11 += lr * error * hn1;
            ow21 += lr * error * hn2;

            hnw11 += lr * error_hn1 * in1;
            hnw12 += lr * error_hn2 * in1;
            hnw21 += lr * error_hn1 * in2;
            hnw22 += lr * error_hn2 * in2;

            show("\nAfter learn: ");
            show();
            //
        }

        public void show()
        {
            Console.Write("\nhw11: " + hnw11 + "\nhw12: " + hnw12 + "\nhw21: " + hnw21 + "\nhw22: " + hnw22 + "\now11: " + ow11 + "\now21: " + ow21 + "\n");
        }
        public void show(string message)
        {
            Console.Write(message);
        }

    }
}
