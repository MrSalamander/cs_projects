﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SSI___Sieci_neuronowe
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Dla przykladu napisałem program obsługujący dwie warstwy ukryte i dwa wejścia. ");

            int[] ins = { 1, 5 };
            decimal target = 1m;

            BackPropagation bp = new BackPropagation();
            bp.train(ins, target);

            ins = new int[]{ 2, 3 };
            bp.train(ins, target);

            Console.ReadKey();
        }
    }
}
