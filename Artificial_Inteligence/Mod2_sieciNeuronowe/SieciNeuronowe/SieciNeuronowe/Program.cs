﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieciNeuronowe
{
	class Program
	{
		static void Main(string[] args)
		{
		//	int[] inputs = { 1, 3 };
		//	Console.WriteLine("\nFirst version of Ex 1.2 with input { 1, 3 }\n");
		//	Zad1_2 first = new Zad1_2(inputs);
		//	Console.WriteLine("\nSecond version of Ex 1.2 with input { 3, 3 }\n");
		//	inputs[0] = 3;
		//	Zad1_2 second = new Zad1_2(inputs);



			List<double[]> inputs2 = new List<double[]>();
			inputs2.Add(new double[3] { 1, 3, 0 });
			inputs2.Add(new double[3] { 1, 3, 1 });
			inputs2.Add(new double[3] { 2, 2, 0 });
			double[] target2 = { 1, 0 };
			Zad2_1 WTA = new Zad2_1(3, 2, 5, 2);

			WTA.showWeights();
			for (int i = 0; i < 1000; i++)
			{
				if (i % 3 == 0)
					WTA.train(inputs2[0], target2);

				else if (i % 3 == 0)
					WTA.train(inputs2[1], target2);
				else

					WTA.train(inputs2[2], target2);
			}

			Console.WriteLine("@@@@@@@@@@@@@@@@@@@@@@@@@@@@@");
			WTA.showWeights();



			Console.ReadKey();



		}
	}
}
