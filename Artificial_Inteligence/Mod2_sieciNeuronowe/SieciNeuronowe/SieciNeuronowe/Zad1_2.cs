﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SieciNeuronowe
{
	class Zad1_2
	{
		int[] inputs;
		double hiddenWeight_11 = 0.11;
		double hiddenWeight_12 = 0.21;
		double hiddenWeight_21 = 0.12;
		double hiddenWeight_22 = 0.08;

		double outputWeight_1 = 0.14;
		double outputWeight_2 = 0.15;
		double learningRate = 0.05;

		public Zad1_2(int[] inputs)
		{
			this.inputs = inputs;
			Console.Write("before training");
			showNN();

			train(this.inputs,1);
			Console.Write("\nFirst age: ");
			showNN();

			train(this.inputs, 1);
			Console.Write("\nSecond age: ");
			showNN();

		}
		private void showNN()
		{
			Console.WriteLine();
			Console.Write("hiddenWeight_11 = " + String.Format("{0:0.###}", hiddenWeight_11));
			Console.WriteLine("        hiddenWeight_12 = " + String.Format("{0:0.###}", hiddenWeight_12));
			Console.Write("hiddenWeight_21 = " + String.Format("{0:0.###}", hiddenWeight_21));
			Console.WriteLine("        hiddenWeight_22 = " + String.Format("{0:0.###}", hiddenWeight_22));

			Console.Write("outputWeight_1  = " + String.Format("{0:0.###}", outputWeight_1));
			Console.WriteLine("        outputWeight_2  = " + String.Format("{0:0.###}", outputWeight_2));

		}

		public void train(int[] inputs, int target)
		{
			//hidden layer 1.
			double hiddenNeuron1 = inputs[0] * hiddenWeight_11 + inputs[1] * hiddenWeight_21;
			double hiddenNeuron2 = inputs[0] * hiddenWeight_12 + inputs[1] * hiddenWeight_22;
			//output layer
			double outNeuron = hiddenNeuron1 * outputWeight_1 + hiddenNeuron2 * outputWeight_2;

			//error counting
			double error = Math.Pow((target - outNeuron), 2);
			double error_HN1 = outputWeight_1 / (outputWeight_1 + outputWeight_2) * error;
			double error_HN2 = outputWeight_2 / (outputWeight_1 + outputWeight_2) * error;

			//learning weights from back
			outputWeight_1 += learningRate * error_HN1 * hiddenNeuron1;
			outputWeight_1 += learningRate * error_HN2 * hiddenNeuron2;

			hiddenWeight_11 += learningRate * error_HN1 * inputs[0];
			hiddenWeight_12 += learningRate * error_HN2 * inputs[0];
			hiddenWeight_21 += learningRate * error_HN1 * inputs[1];
			hiddenWeight_22 += learningRate * error_HN2 * inputs[1];


		}


	}
}
