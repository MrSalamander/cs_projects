﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MathNet.Numerics.LinearAlgebra;

namespace SieciNeuronowe
{
	class Zad2_1
	{
		Random rand = new Random();
		int inputsSize;
		int outputsSize;
		int[] outputs;
		int hiddenLayers;
		int hiddenNeurons;
		double learningRate = 50;

		int[] input;
		
		List<Matrix<double>> weights = new List<Matrix<double>>();
		List<Matrix<double>> bias = new List<Matrix<double>>();



		public Zad2_1(int inputsSize, int hiddenLayers, int hiddenNeurons, int outputsSize)
		{
			this.inputsSize = inputsSize;
			this.hiddenLayers = hiddenLayers;
			this.hiddenNeurons = hiddenNeurons;
			this.outputsSize = outputsSize;
			initialize();

		}

		private void initialize()
		{
			
			if(hiddenLayers == 0)
			{
				weights.Add(Matrix<double>.Build.Random(inputsSize, outputsSize)); //fill random(0-1) matrix |output| x |input|
				bias.Add(Matrix<double>.Build.Random(outputsSize,1));
			}else
			{
				//generate weights and biases for first layer
				weights.Add(Matrix<double>.Build.Random(inputsSize, hiddenNeurons)); //fill random(0-1) matrix |hiddenNeurons| x |input|
				bias.Add(Matrix<double>.Build.Random(hiddenNeurons,1));

				for (int i = 1; i <= hiddenLayers; i++) // for each hidden Layer generate weights
				{
					weights.Add(Matrix<double>.Build.Random(hiddenNeurons, hiddenNeurons)); //fill random(0-1) matrix |hiddenNeurons| x |input|
					bias.Add(Matrix<double>.Build.Random(hiddenNeurons,1));
				}

				//generate weights and biases for last layer
				weights.Add(Matrix<double>.Build.Random(hiddenNeurons, outputsSize)); //fill random(0-1) matrix |hiddenNeurons| x |input|
				bias.Add(Matrix<double>.Build.Random(outputsSize,1));
			}

		}

		public void train(double[] input, double[] target)
		{
			Vector<double> inputs2 = Vector<double>.Build.DenseOfArray(input);
			Vector<double> targets2 = Vector<double>.Build.DenseOfArray(target);
			Matrix<double> inputs = Matrix<double>.Build.DenseOfColumnVectors(inputs2);
			Matrix<double> targets = Matrix<double>.Build.DenseOfColumnVectors(targets2);

			List<Matrix<double>> errors = new List<Matrix<double>>();
			Matrix<double> tmp;

			

			List<Matrix<double>> hidden = new List<Matrix<double>>();
			hidden.Add(inputs);
			tmp = weights[0].Transpose().Multiply(inputs);
			tmp = tmp.Add(bias[0]);
			tmp = sigmoid(tmp);
			hidden.Add(tmp);

			for (int i = 1; i<=hiddenLayers+1;i++)
			{
				tmp = weights[i].Transpose().Multiply(tmp);
				tmp = tmp.Add(bias[i]);
				tmp = sigmoid(tmp);
				hidden.Add(tmp);
			}

			errors.Add(targets - tmp);
			List<int>best_hidden = new List<int>();
			for (int i = 0; i <= hiddenLayers + 2; i++)
			{
				int max = 0;
				for (int j = 1; j <= hidden[i].ColumnCount; j++)
				{
					if(hidden[i].At(max,0)<hidden[j].At(j,0))
					{
						max = j;
					}
					best_hidden.Add(max);
				}
			}

			for (int i = 1; i <= hiddenLayers + 1; i++)
			{
				tmp = weights[hiddenLayers + 2 - i] * errors[i - 1];
				errors.Add(tmp);
			}
			//Console.WriteLine(tmp.ToString());
			errors.Reverse();

			
			for (int i = 0; i <= hiddenLayers+1; i++)
			{
				int x = best_hidden[i + 1];
				
				//Console.WriteLine(errors[i].ToString());


				for (int j = 0; j< weights[i].RowCount; j++)
				{
					//Console.WriteLine(x);
					double delta = learningRate * (errors[i])[x,0];
					delta *= hidden[i + 1].At(x, 0) * (1 - hidden[i + 1].At(x, 0));
					delta *= hidden[i].At(x, 0);
					double temp = weights[i].At(j, x);
					(weights[i])[j, x] = (temp + delta);
					//Console.WriteLine((weights[i])[j, x]);

				}
			}
		}

		public void showWeights()
		{
			for (int i = 0; i < weights.Count(); i++)
			{
				Console.WriteLine(weights[i].ToString());
			}
		}

		private Matrix<double> sigmoid(Matrix<double> x)
		{
			Matrix<double> sigm = 1 / (1 + Matrix<double>.Exp(-x));
			return sigm;
		}
	}
}
