﻿namespace WindowsFormsApp1
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.button1 = new System.Windows.Forms.Button();
			this.button2 = new System.Windows.Forms.Button();
			this.button3 = new System.Windows.Forms.Button();
			this.button4 = new System.Windows.Forms.Button();
			this.button5 = new System.Windows.Forms.Button();
			this.button6 = new System.Windows.Forms.Button();
			this.button99 = new System.Windows.Forms.Button();
			this.button8 = new System.Windows.Forms.Button();
			this.button7 = new System.Windows.Forms.Button();
			this.button0 = new System.Windows.Forms.Button();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.backspaceButton = new System.Windows.Forms.Button();
			this.divideButton = new System.Windows.Forms.Button();
			this.clearFieldButton = new System.Windows.Forms.Button();
			this.signButton = new System.Windows.Forms.Button();
			this.sumButton = new System.Windows.Forms.Button();
			this.confirmButton = new System.Windows.Forms.Button();
			this.clearButton = new System.Windows.Forms.Button();
			this.separatorButton = new System.Windows.Forms.Button();
			this.multiplyButton = new System.Windows.Forms.Button();
			this.substractButton = new System.Windows.Forms.Button();
			this.field = new System.Windows.Forms.TextBox();
			this.signLabel = new System.Windows.Forms.Label();
			this.resultLabel = new System.Windows.Forms.TextBox();
			this.groupBox1.SuspendLayout();
			this.SuspendLayout();
			// 
			// button1
			// 
			this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button1.FlatAppearance.BorderColor = System.Drawing.Color.Teal;
			this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.68317F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.button1.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.button1.Location = new System.Drawing.Point(13, 221);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(76, 63);
			this.button1.TabIndex = 0;
			this.button1.Text = "1";
			this.button1.UseVisualStyleBackColor = false;
			this.button1.Click += new System.EventHandler(this.Button0_Click);
			// 
			// button2
			// 
			this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button2.FlatAppearance.BorderColor = System.Drawing.Color.Teal;
			this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.68317F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.button2.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.button2.Location = new System.Drawing.Point(95, 221);
			this.button2.Name = "button2";
			this.button2.Size = new System.Drawing.Size(76, 63);
			this.button2.TabIndex = 1;
			this.button2.Text = "2";
			this.button2.UseVisualStyleBackColor = false;
			this.button2.Click += new System.EventHandler(this.Button0_Click);
			// 
			// button3
			// 
			this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button3.FlatAppearance.BorderColor = System.Drawing.Color.Teal;
			this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.68317F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.button3.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.button3.Location = new System.Drawing.Point(177, 221);
			this.button3.Name = "button3";
			this.button3.Size = new System.Drawing.Size(76, 63);
			this.button3.TabIndex = 2;
			this.button3.Text = "3";
			this.button3.UseVisualStyleBackColor = false;
			this.button3.Click += new System.EventHandler(this.Button0_Click);
			// 
			// button4
			// 
			this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button4.FlatAppearance.BorderColor = System.Drawing.Color.Teal;
			this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.68317F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.button4.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.button4.Location = new System.Drawing.Point(13, 152);
			this.button4.Name = "button4";
			this.button4.Size = new System.Drawing.Size(76, 63);
			this.button4.TabIndex = 3;
			this.button4.Text = "4";
			this.button4.UseVisualStyleBackColor = false;
			this.button4.Click += new System.EventHandler(this.Button0_Click);
			// 
			// button5
			// 
			this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button5.FlatAppearance.BorderColor = System.Drawing.Color.Teal;
			this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.68317F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.button5.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.button5.Location = new System.Drawing.Point(95, 152);
			this.button5.Name = "button5";
			this.button5.Size = new System.Drawing.Size(76, 63);
			this.button5.TabIndex = 4;
			this.button5.Text = "5";
			this.button5.UseVisualStyleBackColor = false;
			this.button5.Click += new System.EventHandler(this.Button0_Click);
			// 
			// button6
			// 
			this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button6.FlatAppearance.BorderColor = System.Drawing.Color.Teal;
			this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.68317F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.button6.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.button6.Location = new System.Drawing.Point(177, 152);
			this.button6.Name = "button6";
			this.button6.Size = new System.Drawing.Size(76, 63);
			this.button6.TabIndex = 5;
			this.button6.Text = "6";
			this.button6.UseVisualStyleBackColor = false;
			this.button6.Click += new System.EventHandler(this.Button0_Click);
			// 
			// button99
			// 
			this.button99.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button99.FlatAppearance.BorderColor = System.Drawing.Color.Teal;
			this.button99.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button99.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.68317F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.button99.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.button99.Location = new System.Drawing.Point(177, 83);
			this.button99.Name = "button99";
			this.button99.Size = new System.Drawing.Size(76, 63);
			this.button99.TabIndex = 6;
			this.button99.Text = "9";
			this.button99.UseVisualStyleBackColor = false;
			this.button99.Click += new System.EventHandler(this.Button0_Click);
			// 
			// button8
			// 
			this.button8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button8.FlatAppearance.BorderColor = System.Drawing.Color.Teal;
			this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.68317F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.button8.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.button8.Location = new System.Drawing.Point(95, 83);
			this.button8.Name = "button8";
			this.button8.Size = new System.Drawing.Size(76, 63);
			this.button8.TabIndex = 7;
			this.button8.Text = "8";
			this.button8.UseVisualStyleBackColor = false;
			this.button8.Click += new System.EventHandler(this.Button0_Click);
			// 
			// button7
			// 
			this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button7.FlatAppearance.BorderColor = System.Drawing.Color.Teal;
			this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.68317F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.button7.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.button7.Location = new System.Drawing.Point(13, 83);
			this.button7.Name = "button7";
			this.button7.Size = new System.Drawing.Size(76, 63);
			this.button7.TabIndex = 8;
			this.button7.Text = "7";
			this.button7.UseVisualStyleBackColor = false;
			this.button7.Click += new System.EventHandler(this.Button0_Click);
			// 
			// button0
			// 
			this.button0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.button0.FlatAppearance.BorderColor = System.Drawing.Color.Teal;
			this.button0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.button0.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.68317F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.button0.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.button0.Location = new System.Drawing.Point(95, 290);
			this.button0.Name = "button0";
			this.button0.Size = new System.Drawing.Size(76, 63);
			this.button0.TabIndex = 9;
			this.button0.Text = "0";
			this.button0.UseVisualStyleBackColor = false;
			this.button0.Click += new System.EventHandler(this.Button0_Click);
			// 
			// groupBox1
			// 
			this.groupBox1.BackColor = System.Drawing.Color.Transparent;
			this.groupBox1.Controls.Add(this.backspaceButton);
			this.groupBox1.Controls.Add(this.divideButton);
			this.groupBox1.Controls.Add(this.clearFieldButton);
			this.groupBox1.Controls.Add(this.signButton);
			this.groupBox1.Controls.Add(this.sumButton);
			this.groupBox1.Controls.Add(this.confirmButton);
			this.groupBox1.Controls.Add(this.clearButton);
			this.groupBox1.Controls.Add(this.separatorButton);
			this.groupBox1.Controls.Add(this.multiplyButton);
			this.groupBox1.Controls.Add(this.substractButton);
			this.groupBox1.Controls.Add(this.button2);
			this.groupBox1.Controls.Add(this.button0);
			this.groupBox1.Controls.Add(this.button1);
			this.groupBox1.Controls.Add(this.button7);
			this.groupBox1.Controls.Add(this.button3);
			this.groupBox1.Controls.Add(this.button8);
			this.groupBox1.Controls.Add(this.button4);
			this.groupBox1.Controls.Add(this.button99);
			this.groupBox1.Controls.Add(this.button5);
			this.groupBox1.Controls.Add(this.button6);
			this.groupBox1.Cursor = System.Windows.Forms.Cursors.Default;
			this.groupBox1.Location = new System.Drawing.Point(12, 145);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(343, 359);
			this.groupBox1.TabIndex = 10;
			this.groupBox1.TabStop = false;
			// 
			// backspaceButton
			// 
			this.backspaceButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.backspaceButton.FlatAppearance.BorderColor = System.Drawing.Color.Teal;
			this.backspaceButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.backspaceButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 36.35643F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.backspaceButton.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.backspaceButton.Location = new System.Drawing.Point(177, 14);
			this.backspaceButton.Name = "backspaceButton";
			this.backspaceButton.Size = new System.Drawing.Size(76, 63);
			this.backspaceButton.TabIndex = 18;
			this.backspaceButton.Text = "←";
			this.backspaceButton.UseVisualStyleBackColor = false;
			this.backspaceButton.Click += new System.EventHandler(this.backspaceButton_Click);
			// 
			// divideButton
			// 
			this.divideButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.divideButton.FlatAppearance.BorderColor = System.Drawing.Color.Teal;
			this.divideButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.divideButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.divideButton.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.divideButton.Location = new System.Drawing.Point(259, 221);
			this.divideButton.Name = "divideButton";
			this.divideButton.Size = new System.Drawing.Size(74, 63);
			this.divideButton.TabIndex = 14;
			this.divideButton.Text = "/";
			this.divideButton.UseVisualStyleBackColor = false;
			this.divideButton.Click += new System.EventHandler(this.DivideButton_Click);
			// 
			// clearFieldButton
			// 
			this.clearFieldButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.clearFieldButton.FlatAppearance.BorderColor = System.Drawing.Color.Teal;
			this.clearFieldButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.clearFieldButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.clearFieldButton.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.clearFieldButton.Location = new System.Drawing.Point(13, 14);
			this.clearFieldButton.Name = "clearFieldButton";
			this.clearFieldButton.Size = new System.Drawing.Size(76, 63);
			this.clearFieldButton.TabIndex = 17;
			this.clearFieldButton.Text = "CE";
			this.clearFieldButton.UseVisualStyleBackColor = false;
			this.clearFieldButton.Click += new System.EventHandler(this.ClearFieldButton_Click);
			// 
			// signButton
			// 
			this.signButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.signButton.FlatAppearance.BorderColor = System.Drawing.Color.Teal;
			this.signButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.signButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.signButton.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.signButton.Location = new System.Drawing.Point(13, 290);
			this.signButton.Name = "signButton";
			this.signButton.Size = new System.Drawing.Size(76, 63);
			this.signButton.TabIndex = 11;
			this.signButton.Text = "±";
			this.signButton.UseVisualStyleBackColor = false;
			this.signButton.Click += new System.EventHandler(this.SignButton_Click);
			// 
			// sumButton
			// 
			this.sumButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.sumButton.FlatAppearance.BorderColor = System.Drawing.Color.Teal;
			this.sumButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.sumButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.sumButton.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.sumButton.Location = new System.Drawing.Point(259, 14);
			this.sumButton.Name = "sumButton";
			this.sumButton.Size = new System.Drawing.Size(74, 63);
			this.sumButton.TabIndex = 11;
			this.sumButton.Text = "+";
			this.sumButton.UseVisualStyleBackColor = false;
			this.sumButton.Click += new System.EventHandler(this.SumButton_Click);
			// 
			// confirmButton
			// 
			this.confirmButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.confirmButton.FlatAppearance.BorderColor = System.Drawing.Color.Black;
			this.confirmButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.confirmButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.confirmButton.ForeColor = System.Drawing.Color.Black;
			this.confirmButton.Location = new System.Drawing.Point(257, 290);
			this.confirmButton.Name = "confirmButton";
			this.confirmButton.Size = new System.Drawing.Size(76, 63);
			this.confirmButton.TabIndex = 15;
			this.confirmButton.Text = "=";
			this.confirmButton.UseVisualStyleBackColor = false;
			this.confirmButton.Click += new System.EventHandler(this.confirmButton_Click);
			// 
			// clearButton
			// 
			this.clearButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.clearButton.FlatAppearance.BorderColor = System.Drawing.Color.Teal;
			this.clearButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.clearButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.clearButton.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.clearButton.Location = new System.Drawing.Point(95, 14);
			this.clearButton.Name = "clearButton";
			this.clearButton.Size = new System.Drawing.Size(76, 63);
			this.clearButton.TabIndex = 16;
			this.clearButton.Text = "C";
			this.clearButton.UseVisualStyleBackColor = false;
			this.clearButton.Click += new System.EventHandler(this.ClearButton_Click);
			// 
			// separatorButton
			// 
			this.separatorButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.separatorButton.FlatAppearance.BorderColor = System.Drawing.Color.Teal;
			this.separatorButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.separatorButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 22.09901F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.separatorButton.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.separatorButton.Location = new System.Drawing.Point(177, 290);
			this.separatorButton.Name = "separatorButton";
			this.separatorButton.Size = new System.Drawing.Size(76, 63);
			this.separatorButton.TabIndex = 10;
			this.separatorButton.Text = ".";
			this.separatorButton.UseVisualStyleBackColor = false;
			this.separatorButton.Click += new System.EventHandler(this.SeparatorButton_Click);
			// 
			// multiplyButton
			// 
			this.multiplyButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.multiplyButton.FlatAppearance.BorderColor = System.Drawing.Color.Teal;
			this.multiplyButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.multiplyButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.multiplyButton.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.multiplyButton.Location = new System.Drawing.Point(259, 152);
			this.multiplyButton.Name = "multiplyButton";
			this.multiplyButton.Size = new System.Drawing.Size(74, 63);
			this.multiplyButton.TabIndex = 13;
			this.multiplyButton.Text = "*";
			this.multiplyButton.UseVisualStyleBackColor = false;
			this.multiplyButton.Click += new System.EventHandler(this.MultiplyButton_Click);
			// 
			// substractButton
			// 
			this.substractButton.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.substractButton.FlatAppearance.BorderColor = System.Drawing.Color.Teal;
			this.substractButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.substractButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.substractButton.ForeColor = System.Drawing.SystemColors.ControlLight;
			this.substractButton.Location = new System.Drawing.Point(259, 83);
			this.substractButton.Name = "substractButton";
			this.substractButton.Size = new System.Drawing.Size(74, 63);
			this.substractButton.TabIndex = 12;
			this.substractButton.Text = "-";
			this.substractButton.UseVisualStyleBackColor = false;
			this.substractButton.Click += new System.EventHandler(this.SubstractButton_Click);
			// 
			// field
			// 
			this.field.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.field.BackColor = System.Drawing.Color.DarkSlateGray;
			this.field.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.field.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.66337F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.field.ForeColor = System.Drawing.SystemColors.MenuText;
			this.field.Location = new System.Drawing.Point(29, 69);
			this.field.Name = "field";
			this.field.ReadOnly = true;
			this.field.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
			this.field.Size = new System.Drawing.Size(289, 39);
			this.field.TabIndex = 20;
			this.field.Text = "0";
			this.field.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// signLabel
			// 
			this.signLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.signLabel.AutoSize = true;
			this.signLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 19.9604F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.signLabel.Location = new System.Drawing.Point(324, 54);
			this.signLabel.Name = "signLabel";
			this.signLabel.Size = new System.Drawing.Size(0, 31);
			this.signLabel.TabIndex = 22;
			// 
			// resultLabel
			// 
			this.resultLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.resultLabel.BackColor = System.Drawing.Color.DarkSlateGray;
			this.resultLabel.BorderStyle = System.Windows.Forms.BorderStyle.None;
			this.resultLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 25.66337F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.resultLabel.ForeColor = System.Drawing.SystemColors.MenuText;
			this.resultLabel.Location = new System.Drawing.Point(29, 6);
			this.resultLabel.Name = "resultLabel";
			this.resultLabel.ReadOnly = true;
			this.resultLabel.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
			this.resultLabel.Size = new System.Drawing.Size(289, 39);
			this.resultLabel.TabIndex = 23;
			this.resultLabel.Text = "0";
			this.resultLabel.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.DimGray;
			this.ClientSize = new System.Drawing.Size(366, 528);
			this.Controls.Add(this.resultLabel);
			this.Controls.Add(this.signLabel);
			this.Controls.Add(this.field);
			this.Controls.Add(this.groupBox1);
			this.Name = "Form1";
			this.Text = "Kalikator";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.groupBox1.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Button button1;
		private System.Windows.Forms.Button button2;
		private System.Windows.Forms.Button button3;
		private System.Windows.Forms.Button button4;
		private System.Windows.Forms.Button button5;
		private System.Windows.Forms.Button button6;
		private System.Windows.Forms.Button button99;
		private System.Windows.Forms.Button button8;
		private System.Windows.Forms.Button button7;
		private System.Windows.Forms.Button button0;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Button separatorButton;
		private System.Windows.Forms.Button sumButton;
		private System.Windows.Forms.Button substractButton;
		private System.Windows.Forms.Button multiplyButton;
		private System.Windows.Forms.Button divideButton;
		private System.Windows.Forms.Button confirmButton;
		private System.Windows.Forms.Button signButton;
		private System.Windows.Forms.Button clearButton;
		private System.Windows.Forms.TextBox field;
		private System.Windows.Forms.Label signLabel;
		private System.Windows.Forms.TextBox resultLabel;
		private System.Windows.Forms.Button backspaceButton;
		private System.Windows.Forms.Button clearFieldButton;
	}
}

