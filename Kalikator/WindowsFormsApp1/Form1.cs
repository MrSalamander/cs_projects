﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			Variables.decimalSeparator = Convert.ToChar(Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator);
		}

		private void ClearButton_Click(object sender, EventArgs e)
		{
			ClearAll();
		}
		private void ClearAll()
		{
			ClearField();
			Variables.firstUse = true;
			Variables.result = 0;
			Variables.equationSign = "";
			signLabel.Text = " ";
			resultLabel.Text = "0";
		}
		private void ClearField() // clear only field with current value
		{
			Variables.separatorButtonClicked = false;
			Variables.nextNumber = 0;
			field.Text = "0";
		}


		private void Count()
		{
			double.TryParse(field.Text, out Variables.nextNumber);
			try
			{
				switch (Variables.equationSign)
				{

					case "+":
						Variables.result = Variables.result + Variables.nextNumber;
						resultLabel.Text = Variables.result.ToString();
						Variables.separatorButtonClicked = false;
						break;

					case "-":
						Variables.result = Variables.result - Variables.nextNumber;
						resultLabel.Text = Variables.result.ToString();
						Variables.separatorButtonClicked = false;
						break;

					case "*":
						Variables.result = Variables.result * Variables.nextNumber;
						resultLabel.Text = Variables.result.ToString();
						Variables.separatorButtonClicked = false;
						break;

					case "/":
						if (Variables.nextNumber != 0)
						{
							Variables.result = Variables.result / Variables.nextNumber;
							resultLabel.Text = Variables.result.ToString();
							Variables.separatorButtonClicked = false;
						}
						else
						{
							MessageBox.Show("Do not divide by 0");
							ClearAll();
						}
						break;
				}
				if (double.IsInfinity(Variables.result)) throw new OverflowException();
			}
			catch(OverflowException e) { MessageBox.Show("OverflowException, the number was too big"); ClearAll();}
		}
		private void CountCheck()
		{
			if (!Variables.firstUse) { Count();  }
			else
			{
				Variables.firstUse = false;
				resultLabel.Text = field.Text;
				double.TryParse(resultLabel.Text, out Variables.result);
			}
			field.Text = "0";
			ClearField();
		}

		#region EventHandling
		private void Button0_Click(object sender, EventArgs e)
		{
			string number = (sender as Button).Text;
			if (field.Text == "0")
			{
				if (number != "0") { field.Text = ""; field.AppendText(number); }
			}
			else field.AppendText(number);
		}
		private void SeparatorButton_Click(object sender, EventArgs e)
		{
			if (Variables.separatorButtonClicked == false)
			{
				Variables.separatorButtonClicked = true;
				string separator = Convert.ToString(Thread.CurrentThread.CurrentCulture.NumberFormat.NumberDecimalSeparator);
				field.AppendText(separator);
			}
		}
		private void SignButton_Click(object sender, EventArgs e)
		{
			double temp;
			double.TryParse(field.Text, out temp);
			temp *= -1;
			field.Text = temp.ToString();

		}
		private void SumButton_Click(object sender, EventArgs e)
		{
			CountCheck();
			Variables.equationSign = "+";
			signLabel.Text = "+";

		}
		private void SubstractButton_Click(object sender, EventArgs e)
		{
			CountCheck();
			Variables.equationSign = "-";
			signLabel.Text = "-";

		}
		private void MultiplyButton_Click(object sender, EventArgs e)
		{
			CountCheck();
			Variables.equationSign = "*";
			signLabel.Text = "*";

		}
		private void DivideButton_Click(object sender, EventArgs e)
		{
			CountCheck();
			Variables.equationSign = "/";
			signLabel.Text = "/";

		}
		private void confirmButton_Click(object sender, EventArgs e) => CountCheck();
		private void backspaceButton_Click(object sender, EventArgs e)
		{
			string s = field.Text;

			if (s.Length > 1)
			{
				if (s[s.Length - 1] == Variables.decimalSeparator) Variables.separatorButtonClicked = false;
				s = s.Substring(0, s.Length - 1);
			}
			else {s = "0";}
			field.Text = s;
		}
		private void ClearFieldButton_Click(object sender, EventArgs e) => ClearField();
		#endregion
	}
}
