﻿namespace LabNr1
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.splitContainer1 = new System.Windows.Forms.SplitContainer();
			this.UserGroupBox = new System.Windows.Forms.GroupBox();
			this.deleteButton = new System.Windows.Forms.Button();
			this.editButton = new System.Windows.Forms.Button();
			this.ageUpDown = new System.Windows.Forms.NumericUpDown();
			this.cancelButton = new System.Windows.Forms.Button();
			this.AddButton = new System.Windows.Forms.Button();
			this.surnameTextBox = new System.Windows.Forms.TextBox();
			this.nameTextBox = new System.Windows.Forms.TextBox();
			this.SurnameLabel = new System.Windows.Forms.Label();
			this.NameLabel = new System.Windows.Forms.Label();
			this.listBox = new System.Windows.Forms.ListBox();
			this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
			this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
			this.splitContainer1.Panel1.SuspendLayout();
			this.splitContainer1.Panel2.SuspendLayout();
			this.splitContainer1.SuspendLayout();
			this.UserGroupBox.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.ageUpDown)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
			this.SuspendLayout();
			// 
			// splitContainer1
			// 
			this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
			this.splitContainer1.Location = new System.Drawing.Point(0, 0);
			this.splitContainer1.Name = "splitContainer1";
			// 
			// splitContainer1.Panel1
			// 
			this.splitContainer1.Panel1.Controls.Add(this.UserGroupBox);
			// 
			// splitContainer1.Panel2
			// 
			this.splitContainer1.Panel2.Controls.Add(this.listBox);
			this.splitContainer1.Size = new System.Drawing.Size(719, 490);
			this.splitContainer1.SplitterDistance = 342;
			this.splitContainer1.TabIndex = 0;
			// 
			// UserGroupBox
			// 
			this.UserGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.UserGroupBox.Controls.Add(this.deleteButton);
			this.UserGroupBox.Controls.Add(this.editButton);
			this.UserGroupBox.Controls.Add(this.ageUpDown);
			this.UserGroupBox.Controls.Add(this.cancelButton);
			this.UserGroupBox.Controls.Add(this.AddButton);
			this.UserGroupBox.Controls.Add(this.surnameTextBox);
			this.UserGroupBox.Controls.Add(this.nameTextBox);
			this.UserGroupBox.Controls.Add(this.SurnameLabel);
			this.UserGroupBox.Controls.Add(this.NameLabel);
			this.UserGroupBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.UserGroupBox.Location = new System.Drawing.Point(12, 12);
			this.UserGroupBox.Name = "UserGroupBox";
			this.UserGroupBox.Size = new System.Drawing.Size(324, 269);
			this.UserGroupBox.TabIndex = 0;
			this.UserGroupBox.TabStop = false;
			this.UserGroupBox.Text = "User";
			// 
			// deleteButton
			// 
			this.deleteButton.AutoSize = true;
			this.deleteButton.Location = new System.Drawing.Point(176, 213);
			this.deleteButton.Name = "deleteButton";
			this.deleteButton.Size = new System.Drawing.Size(127, 35);
			this.deleteButton.TabIndex = 5;
			this.deleteButton.Text = "Delete";
			this.deleteButton.UseVisualStyleBackColor = true;
			this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
			// 
			// editButton
			// 
			this.editButton.AutoSize = true;
			this.editButton.Location = new System.Drawing.Point(27, 213);
			this.editButton.Name = "editButton";
			this.editButton.Size = new System.Drawing.Size(129, 35);
			this.editButton.TabIndex = 6;
			this.editButton.Text = "Edit";
			this.editButton.UseVisualStyleBackColor = true;
			this.editButton.Click += new System.EventHandler(this.editButton_Click);
			// 
			// ageUpDown
			// 
			this.ageUpDown.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ageUpDown.Location = new System.Drawing.Point(183, 114);
			this.ageUpDown.Minimum = new decimal(new int[] {
            20,
            0,
            0,
            0});
			this.ageUpDown.Name = "ageUpDown";
			this.ageUpDown.Size = new System.Drawing.Size(120, 30);
			this.ageUpDown.TabIndex = 2;
			this.ageUpDown.Value = new decimal(new int[] {
            20,
            0,
            0,
            0});
			this.ageUpDown.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ageUpDown_KeyPress);
			// 
			// cancelButton
			// 
			this.cancelButton.AutoSize = true;
			this.cancelButton.Location = new System.Drawing.Point(27, 162);
			this.cancelButton.Name = "cancelButton";
			this.cancelButton.Size = new System.Drawing.Size(129, 35);
			this.cancelButton.TabIndex = 3;
			this.cancelButton.Text = "Cancel";
			this.cancelButton.UseVisualStyleBackColor = true;
			this.cancelButton.Click += new System.EventHandler(this.CancelButton_Click);
			// 
			// AddButton
			// 
			this.AddButton.AutoSize = true;
			this.AddButton.Location = new System.Drawing.Point(176, 162);
			this.AddButton.Name = "AddButton";
			this.AddButton.Size = new System.Drawing.Size(127, 35);
			this.AddButton.TabIndex = 4;
			this.AddButton.Text = "Add";
			this.AddButton.UseVisualStyleBackColor = true;
			this.AddButton.Click += new System.EventHandler(this.AddButton_Click);
			// 
			// surnameTextBox
			// 
			this.surnameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.surnameTextBox.Location = new System.Drawing.Point(100, 67);
			this.surnameTextBox.Name = "surnameTextBox";
			this.surnameTextBox.Size = new System.Drawing.Size(203, 30);
			this.surnameTextBox.TabIndex = 1;
			// 
			// nameTextBox
			// 
			this.nameTextBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.nameTextBox.Location = new System.Drawing.Point(100, 29);
			this.nameTextBox.Name = "nameTextBox";
			this.nameTextBox.Size = new System.Drawing.Size(203, 30);
			this.nameTextBox.TabIndex = 0;
			// 
			// SurnameLabel
			// 
			this.SurnameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.SurnameLabel.AutoSize = true;
			this.SurnameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.SurnameLabel.Location = new System.Drawing.Point(7, 70);
			this.SurnameLabel.Name = "SurnameLabel";
			this.SurnameLabel.Size = new System.Drawing.Size(98, 25);
			this.SurnameLabel.TabIndex = 1;
			this.SurnameLabel.Text = "Surname:";
			// 
			// NameLabel
			// 
			this.NameLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.NameLabel.AutoSize = true;
			this.NameLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.NameLabel.Location = new System.Drawing.Point(7, 32);
			this.NameLabel.Name = "NameLabel";
			this.NameLabel.Size = new System.Drawing.Size(70, 25);
			this.NameLabel.TabIndex = 0;
			this.NameLabel.Text = "Name:";
			// 
			// listBox
			// 
			this.listBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.listBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.listBox.FormattingEnabled = true;
			this.listBox.ItemHeight = 25;
			this.listBox.Location = new System.Drawing.Point(3, 13);
			this.listBox.Name = "listBox";
			this.listBox.Size = new System.Drawing.Size(353, 454);
			this.listBox.TabIndex = 0;
			this.listBox.SelectedIndexChanged += new System.EventHandler(this.listBox_SelectedIndexChanged);
			// 
			// errorProvider1
			// 
			this.errorProvider1.ContainerControl = this;
			// 
			// contextMenuStrip1
			// 
			this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(17, 17);
			this.contextMenuStrip1.Name = "contextMenuStrip1";
			this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
			// 
			// Form1
			// 
			this.ClientSize = new System.Drawing.Size(719, 490);
			this.Controls.Add(this.splitContainer1);
			this.MinimumSize = new System.Drawing.Size(735, 530);
			this.Name = "Form1";
			this.Text = "Aplikacja nr 1";
			this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
			this.Load += new System.EventHandler(this.Form1_Load);
			this.splitContainer1.Panel1.ResumeLayout(false);
			this.splitContainer1.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
			this.splitContainer1.ResumeLayout(false);
			this.UserGroupBox.ResumeLayout(false);
			this.UserGroupBox.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.ageUpDown)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.SplitContainer splitContainer1;
		private System.Windows.Forms.GroupBox UserGroupBox;
		private System.Windows.Forms.Button cancelButton;
		private System.Windows.Forms.Button AddButton;
		private System.Windows.Forms.TextBox surnameTextBox;
		private System.Windows.Forms.TextBox nameTextBox;
		private System.Windows.Forms.Label SurnameLabel;
		private System.Windows.Forms.Label NameLabel;
		private System.Windows.Forms.ErrorProvider errorProvider1;
		private System.Windows.Forms.NumericUpDown ageUpDown;
		private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
		private System.Windows.Forms.Button editButton;
		private System.Windows.Forms.ListBox listBox;
		private System.Windows.Forms.Button deleteButton;
	}
}

