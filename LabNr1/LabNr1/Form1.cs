﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace LabNr1
{
	public partial class Form1 : Form
	{
		public Form1()
		{
			InitializeComponent();
		}

		private void Form1_Load(object sender, EventArgs e)
		{
			Console.WriteLine("dupa");
			try
			{ 
				using (StreamReader sr = new StreamReader(Statics.userFilePath))
				{
					string[] words;
					string line;
					uint age;
					while ((line = sr.ReadLine()) != null)
					{
						words = line.Split(' ');
						if (UInt32.TryParse(words[2], out age)) { listBox.Items.Add(new User(words[0], words[1], age)); }
						}
				}
			}
			catch (Exception er)
			{
				// Let the user know what went wrong.
				Console.WriteLine("The file could not be read:");
				Console.WriteLine(er.Message);
			}
		}

		private void button1_Click(object sender, EventArgs e)
		{
			NameLabel.Text = "Hello my friend!";
		}

		private void AddButton_Click(object sender, EventArgs e)
		{
			Button bt = (Button)sender;
			if (nameTextBox.Text=="")
			{
				errorProvider1.SetError(nameTextBox, "puste Pole");
			}else
			if (surnameTextBox.Text == "")
			{
				errorProvider1.SetError(surnameTextBox, "puste Pole");
			}
			else
			{
				listBox.Items.Add(new User(nameTextBox.Text, surnameTextBox.Text, (uint)ageUpDown.Value));
				ClearForm();
			}

		}

		private void CancelButton_Click(object sender, EventArgs e)
		{
			ClearForm();
		}

		private void ClearForm()
		{
			nameTextBox.Text = "";
			surnameTextBox.Text = "";
			ageUpDown.Value = 20;

		}

		private void listBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			User temp = listBox.SelectedItem as User;
			if (temp != null)
			{ 	
			nameTextBox.Text = temp.firstName;
			surnameTextBox.Text = temp.lastName;
			ageUpDown.Value = temp.age;
			}
		}

		private void editButton_Click(object sender, EventArgs e)
		{
			User temp = listBox.SelectedItem as User;

		}

		private void deleteButton_Click(object sender, EventArgs e)
		{
			listBox.Items.RemoveAt(this.listBox.SelectedIndex);
			listBox.SelectedIndex = -1;
			ClearForm();
		}

		private void Form1_FormClosing(object sender, FormClosingEventArgs e)
		{
			StreamWriter sw = new StreamWriter(Statics.userFilePath);
			foreach(var u in listBox.Items)
			{
				User temp = u as User;
				sw.WriteLine(temp.ToString());
			}
			sw.Close();
		}

		private void ageUpDown_KeyPress(object sender, KeyPressEventArgs e)
		{
			if (e.KeyChar == 13) AddButton_Click(sender, e);
		}
	}
}
