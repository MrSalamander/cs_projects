﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LabNr1
{
	class User
	{
		public string firstName { get; set; }
		public string lastName { get; set; }
		public uint age { get; set; }
		
		public User(string firstName, string lastName, uint age)
		{
			this.firstName = firstName;
			this.lastName = lastName;
			this.age = age;
		}

		public override string ToString()
		{	
			return this.firstName + " " + this.lastName + " " + this.age;
		}
	}
}
