﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Forms;

namespace Mini_Total_Commander.Model
{
	class Model1
	{

		private List<string> drives;
		//private string path;
		private List<string> files;
		#region Constructor
		public Model1()
		{

		}
		#endregion
		#region Properties
		public List<string> Drives
		{
			get => drives;
			set => drives = value;
		}

		public string Path{ get; set; }

		public List<string> Files {
			get => files;
			set => files = value;
		}
		#endregion
		public List<string> GetFiles(string path) 
		{
			if(path != null && path != "")
			updateFiles(path);
			return Files;
		}

		public List<string> GetDrives()
		{
			updateDrives();
			return Drives;
		}

		public string GetParent(string path)
		{
			Console.WriteLine(path);
			try
			{
				if (!Drives.Contains(path))
					Path = Directory.GetParent(path).ToString();
				else Path = path;
				Console.WriteLine(Path);
			}
			catch (Exception exc) { Console.WriteLine(exc.Message); Path = path; }
			return Path;
		}

		public void Move(string source, string target)
		{
			Console.WriteLine($"przenoszę : {source} , {target}");
			try
			{
				if (Copy(source, target)) Delete(source);
				else Console.WriteLine("nie udało się przenieść");
			}
			catch (Exception exc) { MessageBox.Show(exc.Message); }
		}

		public Boolean Copy(string source, string target)
		{
			if (target != source && source != "" && target != "")
			{
				try
				{

					if (File.Exists(source))
					{
						string tmp = target + "\\" + System.IO.Path.GetFileName(source);
						File.Copy(source, tmp);
						Console.WriteLine($"kopiuje plik {source} -> {tmp}");
						return true;
					}
					else
					if (Directory.Exists(source))
					{
						
						if (System.IO.Path.GetFullPath(source) != System.IO.Path.GetFullPath(target))
						{
							string tmp = target + "\\" + System.IO.Path.GetFileName(source);
							Console.WriteLine($"kopiuje folder {source} -> {tmp}");
							var diSource = new DirectoryInfo(source);
							var diTarget = new DirectoryInfo(tmp);
							CopyAll(diSource, diTarget);
							return true;
						}
						else
						{
							MessageBox.Show("docelowy folder jest tym samym folderem");
							return false;
						}
					}
					else return false;
				}
				catch (Exception exc) { Console.WriteLine(exc.Message + " Model error"); Console.WriteLine(exc.Message); return false; };
			}
			else {
				MessageBox.Show("proszę wybrać poprawne miejsce do kopiowania");
				return false;
			}
		}

		public Boolean Delete(string source)
		{
			if(source != null)
			try
			{
				FileAttributes attr = File.GetAttributes(source);
				if (attr.HasFlag(FileAttributes.Directory))
				{
					Directory.Delete(source, true);
					return true;
				}
				else
				{
					FileInfo file = new FileInfo(source);
					file.Delete();
					return true;
				}
			}
			catch (Exception exc) { MessageBox.Show(exc.Message); }
			return false;
		}

		#region Addictional Methods
		private void updateFiles(string Path)
		{
			if (IsDirectory(Path))
			{
				List<string> AllFiles = new List<string>();
				try
				{
					foreach (string directory in Directory.GetDirectories(Path)) AllFiles.Add(new FileInfo(directory).Name);
					foreach (string file in Directory.GetFiles(Path)) AllFiles.Add(new FileInfo(file).Name);
				}
				catch (Exception e) { Console.WriteLine(e.Message); }
				//Console.WriteLine($"update plików w ścieżce: {Path}");
				Files = AllFiles;
			}
			else
			{
				Execute(Path);
			}
			
		}
		private void updateDrives()
		{
			DriveInfo[] driveinfo = DriveInfo.GetDrives();
			List<string> readyDrives = new List<string>();
			foreach (DriveInfo drive in driveinfo)
			{
				if (drive.IsReady)
				{
					readyDrives.Add(drive.ToString());
				}
			}
			Drives = readyDrives;
		}
		public Boolean IsDirectory(string Path)
		{
			try
			{
				FileAttributes attr = File.GetAttributes(Path);
				if (attr.HasFlag(FileAttributes.Directory)) return true;
				else return false;
			}
			catch (Exception exc) { return false; }
		}
		private void Execute(string Path)
		{
			try
			{
				System.Diagnostics.Process.Start(Path);
			}
			catch (FileNotFoundException) { MessageBox.Show("nie można znaleźć tego pliku", "FileNotFound"); }
			catch (Exception exc) { MessageBox.Show(exc.Message, "Exception"); }
		}
		private void CopyAll(DirectoryInfo source, DirectoryInfo target)
		{
			Console.WriteLine(target.FullName);
			Directory.CreateDirectory(target.FullName);

			// Copy each file into the new directory.
			foreach (FileInfo fi in source.GetFiles())
			{
				fi.CopyTo(System.IO.Path.Combine(target.FullName, fi.Name), true);
			}

			// Copy each subdirectory using recursion.
			foreach (DirectoryInfo diSourceSubDir in source.GetDirectories())
			{
				DirectoryInfo nextTargetSubDir = target.CreateSubdirectory(diSourceSubDir.Name);
				CopyAll(diSourceSubDir, nextTargetSubDir);
			}
		}
		#endregion
	}
}
