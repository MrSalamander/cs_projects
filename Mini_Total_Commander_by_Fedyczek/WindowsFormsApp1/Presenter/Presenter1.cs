﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mini_Total_Commander.Model;
using Mini_Total_Commander.Viev;

namespace Mini_Total_Commander.Presenter
{
	class Presenter1
	{
		Model.Model1 model = new Model.Model1();
		Viev.IView vievForm = new Viev.VievForm();

		public Presenter1(Model1 model, VievForm vievForm)
		{
			this.model = model;
			this.vievForm = vievForm;

			//events for controll
			vievForm.GetDrives += getDrives;
			vievForm.GetFiles += getFiles;
			vievForm.GetParentPath += getParent;

			//operate buttons
			vievForm.Move += move;
			vievForm.Copy += copy;
			vievForm.Delete += delete;

			//extra events
			//vievForm.IsDirectory += isDirectory;
			vievForm.LeftControll.IsDirectory += isDirectory;
			vievForm.RightControll.IsDirectory += isDirectory;
		}

		private List<string> getDrives() => model.GetDrives();
		private List<string> getFiles(string Path) => model.GetFiles(Path);
		private string getParent(string Path) => model.GetParent(Path);

		private void move(string Source, string Target) => model.Move(Source, Target);
		private Boolean copy(string Source, string Target) => model.Copy(Source, Target);
		private Boolean delete(string Source) => model.Delete(Source);

		private Boolean isDirectory(string Path) => model.IsDirectory(Path);
	}
}
