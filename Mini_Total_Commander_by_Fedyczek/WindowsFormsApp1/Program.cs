﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mini_Total_Commander
{
	static class Program
	{
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main()
		{
			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Model.Model1 model = new Model.Model1();
			Viev.VievForm vievForm = new Viev.VievForm();
			Presenter.Presenter1 presenter = new Presenter.Presenter1(model, vievForm);
			Application.Run(vievForm);
		}
	}
}
