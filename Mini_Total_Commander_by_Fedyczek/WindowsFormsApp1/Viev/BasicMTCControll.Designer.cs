﻿namespace Mini_Total_Commander.Viev
{
	partial class BasicMTCControll
	{
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Component Designer generated code

		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.drivesNameComboBox = new System.Windows.Forms.ComboBox();
			this.listBox1 = new System.Windows.Forms.ListBox();
			this.fullPathBox = new System.Windows.Forms.TextBox();
			this.returnButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// drivesNameComboBox
			// 
			this.drivesNameComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.drivesNameComboBox.BackColor = System.Drawing.Color.Teal;
			this.drivesNameComboBox.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.drivesNameComboBox.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
			this.drivesNameComboBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.11881F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.drivesNameComboBox.FormattingEnabled = true;
			this.drivesNameComboBox.Location = new System.Drawing.Point(361, 13);
			this.drivesNameComboBox.Name = "drivesNameComboBox";
			this.drivesNameComboBox.Size = new System.Drawing.Size(83, 28);
			this.drivesNameComboBox.TabIndex = 0;
			this.drivesNameComboBox.SelectedIndexChanged += new System.EventHandler(this.driverNameComboBox_SelectedIndexChanged);
			this.drivesNameComboBox.Click += new System.EventHandler(this.drivesComboBox_Click);
			// 
			// listBox1
			// 
			this.listBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.listBox1.BackColor = System.Drawing.Color.DarkCyan;
			this.listBox1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.listBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.11881F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.listBox1.FormattingEnabled = true;
			this.listBox1.ItemHeight = 20;
			this.listBox1.Location = new System.Drawing.Point(17, 76);
			this.listBox1.Name = "listBox1";
			this.listBox1.Size = new System.Drawing.Size(427, 502);
			this.listBox1.TabIndex = 1;
			this.listBox1.DoubleClick += new System.EventHandler(this.listBox1_DoubleClick);
			// 
			// fullPathBox
			// 
			this.fullPathBox.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.fullPathBox.BackColor = System.Drawing.Color.Teal;
			this.fullPathBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
			this.fullPathBox.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.11881F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.fullPathBox.Location = new System.Drawing.Point(17, 13);
			this.fullPathBox.Name = "fullPathBox";
			this.fullPathBox.ReadOnly = true;
			this.fullPathBox.Size = new System.Drawing.Size(338, 27);
			this.fullPathBox.TabIndex = 2;
			// 
			// returnButton
			// 
			this.returnButton.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.returnButton.BackColor = System.Drawing.Color.LightSeaGreen;
			this.returnButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.returnButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.267326F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.returnButton.ForeColor = System.Drawing.Color.Black;
			this.returnButton.Location = new System.Drawing.Point(17, 47);
			this.returnButton.Name = "returnButton";
			this.returnButton.Size = new System.Drawing.Size(257, 24);
			this.returnButton.TabIndex = 3;
			this.returnButton.Text = "←";
			this.returnButton.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			this.returnButton.UseVisualStyleBackColor = false;
			this.returnButton.Click += new System.EventHandler(this.returnButton_Click);
			// 
			// BasicMTCControll
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
			this.BackColor = System.Drawing.Color.DarkSlateGray;
			this.Controls.Add(this.returnButton);
			this.Controls.Add(this.fullPathBox);
			this.Controls.Add(this.listBox1);
			this.Controls.Add(this.drivesNameComboBox);
			this.Name = "BasicMTCControll";
			this.Size = new System.Drawing.Size(462, 600);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ComboBox drivesNameComboBox;
		private System.Windows.Forms.ListBox listBox1;
		private System.Windows.Forms.TextBox fullPathBox;
		private System.Windows.Forms.Button returnButton;
	}
}
