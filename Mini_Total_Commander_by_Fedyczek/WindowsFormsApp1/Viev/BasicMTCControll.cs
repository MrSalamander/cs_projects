﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mini_Total_Commander.Viev
{
	public partial class BasicMTCControll : UserControl, IBasicMTCControll
	{
		public BasicMTCControll()
		{
			InitializeComponent();
		}
		#region Properites
		public List<string> Drives
		{
			get
			{
				List<string> drives = new List<string>();
				foreach (string item in drivesNameComboBox.Items)
				{
					drives.Add(item);
				}
				return drives;
			}
			set
			{
				drivesNameComboBox.Items.Clear();
				foreach(string item in value)
				{
					drivesNameComboBox.Items.Add(item);
					//Console.WriteLine(item);
				}
			}
		}	
		public List<string> Files
		{
			get
			{
				List<string> files = new List<string>();
				foreach (string file in listBox1.Items) files.Add(file);
				return files;
			}
			set
			{
				listBox1.Items.Clear();
				if(value != null)foreach (string file in value ) listBox1.Items.Add(file);
			}
		}
		public string Path
		{
			get => fullPathBox.Text;
			set => fullPathBox.Text = value;
		}
		public string SelectedItem { get; set; }
		public string SelectedItemPath
		{
			get
			{
				if (Path != null)
				{
					//if (!(listBox1.SelectedIndex < -1))
					//	try { return Path + "\\" + listBox1.SelectedItem.ToString(); }
					//	catch (Exception) { return null; }
					try { if (Drives.Contains(Path)) return Path + listBox1.SelectedItem.ToString();
						else return Path + "\\" + listBox1.SelectedItem.ToString(); }
					catch (Exception) { return null; }

				}
				else return null;
			}
			set => SelectedItemPath = value;
		}
		#endregion

		#region Event Handling
		public event EventHandler UpdateDrives;
		public event EventHandler UpdateFiles;
		public event EventHandler ListBoxDoubleClick;
		public event EventHandler GetParent;
		public event Func<string, Boolean> IsDirectory;

		private void returnButton_Click(object sender, EventArgs e)
		{
			if (GetParent != null)
				GetParent(this, e);
		}
		
		private void driverNameComboBox_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				Path = drivesNameComboBox.SelectedItem.ToString();
				this.UpdateFiles?.Invoke(this, e);
			}
			catch(Exception exception) { Console.WriteLine(exception.ToString()); }
		}

		private void drivesComboBox_Click(object sender, EventArgs e)
		{
			this.UpdateDrives?.Invoke(this, e);
		}

		private void listBox1_DoubleClick(object sender, EventArgs e)
		{
			if (this.ListBoxDoubleClick != null && listBox1.SelectedItem != null)
			{
				SelectedItem = listBox1.SelectedItem.ToString();
				this.ListBoxDoubleClick(this, e);

				bool v = IsDirectory((Path + "\\" + SelectedItem));
				if (v)
				{
					Console.WriteLine("jest folderem");
					Path = Path + "\\" + SelectedItem;
				}
				else Console.WriteLine("nie jest folderem");
			}
		}

		#endregion

	}
}
