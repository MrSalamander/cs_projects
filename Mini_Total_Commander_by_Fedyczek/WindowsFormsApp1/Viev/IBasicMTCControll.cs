﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mini_Total_Commander.Viev
{
	public interface IBasicMTCControll
	{
		#region Properties
		List<string> Drives { get; set; }
		List<string> Files { get; set; }
		string Path { get; set; }
		string SelectedItemPath { get; set; }
		#endregion

		#region Events
		event EventHandler UpdateDrives;
		event EventHandler UpdateFiles;
		event EventHandler ListBoxDoubleClick;
		event EventHandler GetParent;
		event Func<string, Boolean> IsDirectory;
		#endregion
	}
}
