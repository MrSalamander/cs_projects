﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Mini_Total_Commander.Viev
{
	public interface IView
	{

		#region Properties
		List<string> Drives { get; set; }
		IBasicMTCControll LeftControll { get;}
		IBasicMTCControll RightControll { get;}
		#endregion

		#region Events
		event Func<List<string>> GetDrives;
		event Func<string, List<string>> GetFiles;
		event Func<string, string> GetParentPath;
		event Func<string, string, Boolean> Copy;
		event Action<string, string> Move;
		event Func<string,Boolean> Delete;
		#endregion

	}
}
