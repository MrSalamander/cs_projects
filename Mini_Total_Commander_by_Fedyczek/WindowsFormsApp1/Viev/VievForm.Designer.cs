﻿namespace Mini_Total_Commander.Viev
{
	partial class VievForm
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(VievForm));
			this.leftControll = new Mini_Total_Commander.Viev.BasicMTCControll();
			this.rightControll = new Mini_Total_Commander.Viev.BasicMTCControll();
			this.moveButton = new System.Windows.Forms.Button();
			this.copyButton = new System.Windows.Forms.Button();
			this.deleteButton = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// leftControll
			// 
			this.leftControll.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
			this.leftControll.AutoSize = true;
			this.leftControll.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
			this.leftControll.BackColor = System.Drawing.Color.DarkSlateGray;
			this.leftControll.Drives = ((System.Collections.Generic.List<string>)(resources.GetObject("leftControll.Drives")));
			this.leftControll.Files = ((System.Collections.Generic.List<string>)(resources.GetObject("leftControll.Files")));
			this.leftControll.Location = new System.Drawing.Point(12, 12);
			this.leftControll.Name = "leftControll";
			this.leftControll.Path = "";
			this.leftControll.Size = new System.Drawing.Size(348, 558);
			this.leftControll.TabIndex = 0;
			// 
			// rightControll
			// 
			this.rightControll.AutoSize = true;
			this.rightControll.AutoValidate = System.Windows.Forms.AutoValidate.EnableAllowFocusChange;
			this.rightControll.BackColor = System.Drawing.Color.DarkSlateGray;
			this.rightControll.Drives = ((System.Collections.Generic.List<string>)(resources.GetObject("rightControll.Drives")));
			this.rightControll.Files = ((System.Collections.Generic.List<string>)(resources.GetObject("rightControll.Files")));
			this.rightControll.Location = new System.Drawing.Point(448, 12);
			this.rightControll.Name = "rightControll";
			this.rightControll.Path = "";
			this.rightControll.Size = new System.Drawing.Size(348, 558);
			this.rightControll.TabIndex = 1;
			// 
			// moveButton
			// 
			this.moveButton.BackColor = System.Drawing.Color.LightSeaGreen;
			this.moveButton.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
			this.moveButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.moveButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.11881F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.moveButton.Location = new System.Drawing.Point(366, 88);
			this.moveButton.Name = "moveButton";
			this.moveButton.Size = new System.Drawing.Size(75, 46);
			this.moveButton.TabIndex = 2;
			this.moveButton.Text = "Move";
			this.moveButton.UseVisualStyleBackColor = false;
			this.moveButton.Click += new System.EventHandler(this.moveButton_Click);
			// 
			// copyButton
			// 
			this.copyButton.BackColor = System.Drawing.Color.LightSeaGreen;
			this.copyButton.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
			this.copyButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.copyButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.11881F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.copyButton.Location = new System.Drawing.Point(366, 140);
			this.copyButton.Name = "copyButton";
			this.copyButton.Size = new System.Drawing.Size(75, 46);
			this.copyButton.TabIndex = 3;
			this.copyButton.Text = "Copy";
			this.copyButton.UseVisualStyleBackColor = false;
			this.copyButton.Click += new System.EventHandler(this.copyButton_Click);
			// 
			// deleteButton
			// 
			this.deleteButton.BackColor = System.Drawing.Color.LightSeaGreen;
			this.deleteButton.FlatAppearance.BorderColor = System.Drawing.Color.DimGray;
			this.deleteButton.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.deleteButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.11881F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
			this.deleteButton.Location = new System.Drawing.Point(366, 192);
			this.deleteButton.Name = "deleteButton";
			this.deleteButton.Size = new System.Drawing.Size(75, 46);
			this.deleteButton.TabIndex = 4;
			this.deleteButton.Text = "Delete";
			this.deleteButton.UseVisualStyleBackColor = false;
			this.deleteButton.Click += new System.EventHandler(this.deleteButton_Click);
			// 
			// VievForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.Gray;
			this.ClientSize = new System.Drawing.Size(808, 582);
			this.Controls.Add(this.deleteButton);
			this.Controls.Add(this.copyButton);
			this.Controls.Add(this.moveButton);
			this.Controls.Add(this.rightControll);
			this.Controls.Add(this.leftControll);
			this.Name = "VievForm";
			this.Text = "VievForm";
			this.Load += new System.EventHandler(this.onLoad);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		
		private System.Windows.Forms.Button moveButton;
		private System.Windows.Forms.Button copyButton;
		private System.Windows.Forms.Button deleteButton;
	}
}