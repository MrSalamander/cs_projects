﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Mini_Total_Commander.Viev
{
	public partial class VievForm : Form, IView
	{
		public VievForm()
		{

			// to jeest rola prezentera, np jego dodatkowej klasy controll_presenter

			InitializeComponent();
			leftControll.UpdateDrives += new EventHandler(Controll_UpdateDrives);
			leftControll.UpdateFiles += new EventHandler(Controll_UpdateFiles);
			leftControll.ListBoxDoubleClick += new EventHandler(Controll_DoubleClickOnList);
			leftControll.GetParent += new EventHandler(Controll_ReturnButtonClick);
			//leftControll.IsDirectory += Controll_IsDirectory;

			rightControll.UpdateDrives += new EventHandler(Controll_UpdateDrives);
			rightControll.UpdateFiles += new EventHandler(Controll_UpdateFiles);
			rightControll.ListBoxDoubleClick += new EventHandler(Controll_DoubleClickOnList);
			rightControll.GetParent += new EventHandler(Controll_ReturnButtonClick);
		}
		#region Fields
		private BasicMTCControll leftControll;
		private BasicMTCControll rightControll;
		#endregion

		#region Properties
		public List<string> Drives {
			get => leftControll.Drives;
			set
				{
				leftControll.Drives = value;
				rightControll.Drives = value;
				}
		}

		//public IBasicMTCControll leftControll1
		//{
		//	get => leftControll; 
		//	set => leftControll = (BasicMTCControll)value;
		//}
		//public IBasicMTCControll rightControll1
		//{
		//	get => rightControll;
		//	set => rightControll = (BasicMTCControll)value;
		//}

		#endregion
		public IBasicMTCControll LeftControll {
			get { return this.leftControll; }
		}
		public IBasicMTCControll RightControll{
			get	{ return rightControll;	}
		}
		#region Event Handling
		public event Func<List<string>> GetDrives;
		public event Func<string, List<string>> GetFiles;
		public event Func<string, string> GetParentPath;
		public event Func<string, string, Boolean> Copy;
		public event Action<string, string> Move;
		public event Func<string, Boolean> Delete;
		public Func<string, Boolean> IsDirectory;



		private void onLoad(object sender, EventArgs e)
		{
			Drives = GetDrives();
		}
		private Boolean Controll_IsDirectory(string path)
		{
			if (IsDirectory != null)
			{
				this.IsDirectory(path);
				return true;
			}
			else return false;
		}
		private void Controll_UpdateDrives(object sender, EventArgs e)
		{
			((BasicMTCControll)sender).Drives = GetDrives();
		}
		private void Controll_UpdateFiles(object sender, EventArgs e)
		{
			((BasicMTCControll)sender).Files = GetFiles(((BasicMTCControll)sender).Path);
		}
		private void Controll_DoubleClickOnList(object sender, EventArgs e)
		{
			((BasicMTCControll)sender).Files = GetFiles(((BasicMTCControll)sender).Path + "\\" +((BasicMTCControll)sender).SelectedItem);
		}
		private void Controll_ReturnButtonClick(object sender, EventArgs e)
		{
			((BasicMTCControll)sender).Path = GetParentPath(((BasicMTCControll)sender).Path);
			((BasicMTCControll)sender).Files = GetFiles(((BasicMTCControll)sender).Path);
		}

		#endregion

		private void moveButton_Click(object sender, EventArgs e)
		{
			Move?.Invoke(leftControll.SelectedItemPath, rightControll.Path);
			Controll_UpdateFiles(rightControll, e);
			Controll_UpdateFiles(leftControll, e);
		}

		private void copyButton_Click(object sender, EventArgs e)
		{
			Copy?.Invoke(leftControll.SelectedItemPath, rightControll.Path);
			Controll_UpdateFiles(rightControll,e);
		}

		private void deleteButton_Click(object sender, EventArgs e)
		{
			if (Delete != null)
				if (Delete(leftControll.SelectedItemPath))
				{
					Controll_UpdateFiles(rightControll, e);
					Controll_UpdateFiles(leftControll, e);
				}
		}
	}
}
